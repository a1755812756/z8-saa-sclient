# 众帮 成为千万用户小帮手

#### 介绍
众帮SaaS桌面端应用程序
[众帮SaaS云平台](https://www.zrplat.com/)
#### 软件架构
多租户、单体应用


#### 安装教程

1.  下载地址:[点击此处下载](https://gitee.com/a1755812756/z8-saa-sclient/releases)

#### 使用说明

1.  云平台用户打开既用
2.  私有化部署用户 打开文件目录下appsettings.json，将WebUrl改成你内网系统访问地址
    ![输入图片说明](https://foruda.gitee.com/images/1659962822505985576/屏幕截图.png "屏幕截图.png")
3.  CPU卡用户或不需要写卡用户 打开文件目录下appsettings.json，将IsCPU改成true.
    需要发卡用户则设置false
    ![输入图片说明](https://foruda.gitee.com/images/1659962947720681677/屏幕截图.png "屏幕截图.png")
#### 参与贡献


#### 特技

